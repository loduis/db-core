import { Model } from './model';
const TABLE = 'addresses';

export class Address extends Model {
    protected maxCache = 1000;
    static async findOrCreate (hash: string) {
        return (new Address()).findOrCreate(hash, (hash: string) => `SELECT get_address_id_by_name('${hash}') as id`);
    }

    static async *chunk(limit = 100, start = 0) {
        yield* Model.chunk(TABLE, limit, start);
    }
}
