import { Database } from './database';

export default new Database(
    `${process.env.DB_CONNECTOR}:dbname=${process.env.DB_NAME};host=${process.env.DB_HOST}`,
    process.env.DB_USERNAME as string,
    process.env.DB_PASSWORD as string
);

