import db from './handler';

export class Model {
    protected CACHE: any = {};
    protected maxCache: number = 0;

    constructor () {
        this.CACHE = {
            length: 0
        };
    }

    async findOrCreate (hash: string, callback: Function) {
        let _id = this.CACHE[hash];
        if (_id) {
            this.flush();
            return _id;
        }
        let [ {id } ] = await db.query(callback(hash));
        this.CACHE.length ++;
        return this.CACHE[hash] = id;
    }

    flush() {
        if (this.CACHE.length >= this.maxCache) {
            this.CACHE = {
                length: 0
            };
        }
    }

    static async query (sql: string, options = {}) {
        return db.query(sql, options);
    }

    static async* chunk(table: string, limit: number, start: number) {
        for (let row of await Model.query(`select * from ${table} limit ${start}, ${limit}`)) {
            yield row;
            start ++;
        }
        yield* Model.chunk(table, limit, start);
    }
}
