import db from './handler';

export class Transaction {
    static async create (data: any) {
        let {address, value, time, block, hash} = data;
        let [ {id } ] = await db.query(
            `SELECT import_transaction('${hash}', '${address}', ${value}, ${time}, ${block}) as id`
        );
        return id;
    }
}
