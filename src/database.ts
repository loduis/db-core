const Sequelize = require('sequelize');

export class Database {
    private client: any;

    constructor(dsn: string, username: string, password: string) {
        let host = '127.0.0.1';
        let port= 3306;
        let dbname;
        let [dialect, parts] = dsn.split(':');
        for (let part of parts.split(';')) {
            let [key, value] = part.split('=');
            if (key === 'dbname') {
                dbname = value;
            } else if (key == 'host') {
                host = value;
            } else if (key == 'port') {
                port = parseInt(value);
            }
        }
        this.client = new Sequelize(dbname, username, password, {
            host,
            port,
            dialect,
            operatorsAliases: Sequelize.Op,
            logging: false,
            dialectOptions: {
                multipleStatements: true
            }
        });
    }

    async query (sql: string | Array<string>, options: any = null) {
        if (Array.isArray(sql)) {
            sql = sql.join(';');
        }
        if (sql.toLowerCase().includes('select')) {
            options = { type: this.client.QueryTypes.SELECT };
        }
        const self = this;
        return self.client.query(sql, options);
    }
}
