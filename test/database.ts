import { expect } from 'chai';
import { Database } from '../src';

context('Blockchain', () => {
    it ('Should get data from db', async () => {
        const db = new Database(
            `mysql:dbname=${(process.env as any).DB_NAME}`,
            (process.env as any).DB_USERNAME,
            (process.env as any).DB_PASSWORD
        );
        await db.query('select count(*) as num from addresses');
    });
});
