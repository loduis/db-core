interface Database {
    query (sql: string, options: any ): Promise<any>;
}
